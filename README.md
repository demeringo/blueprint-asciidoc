# blueprint-asciidoc

A demo of documentation using asciidoc. Also shows export and publish to various formats.

## Content

- Sample document to show syntax [sample.adoc](sample.adoc)
- Sample reveal.js presentation in asciidoc [sample-revealjs.adoc](sample-revealjs.adoc)
- Installing a local asciidoc instance: [Use_asciidoc.adoc](Use_asciidoc.adoc)
- Automate generation of pdf and html as project artifacts with gitlabci file [.gitlab-ci.yml](.gitlab-ci.yml)

## Local usage

The easiest way to get started is to use asciidoctor as a docker image. gitlab-ci.yml gives examples of conversions.

- [docker-asciidoctor/README.adoc at main · asciidoctor/docker-asciidoctor](https://github.com/asciidoctor/docker-asciidoctor/blob/main/README.adoc)

## Useful extensions for vs code

- [AsciiDoc&#32;-&#32;Visual&#32;Studio&#32;Marketplace](https://marketplace.visualstudio.com/items?itemName=asciidoctor.asciidoctor-vscode)
- [AsciiDoc&#32;Slides&#32;-&#32;Visual&#32;Studio&#32;Marketplace](https://marketplace.visualstudio.com/items?itemName=flobilosaurus.vscode-asciidoc-slides)

## Revealjs / asciidoc tips

`:revealjs_theme: solarized`

- default themes: [reveal.js](https://visualcomputing.github.io/colors/#/themes)
- [Générer vos slides avec Reveal.js et Asciidoctor](https://dev-mind.fr/blog/2018/slide_avec_revealjs_asciidoctor.html)
- [Reveal my Asciidoc : a deep dive into presentation-as-code](https://zenika.github.io/adoc-presentation-model/reveal-my-asciidoc.html)
- [Stunning Presentations with Asciidoctor and RevealJS](https://dev.to/rprabhu/stunning-presentations-with-asciidoctor-and-revealjs-1d1m)

## About Asciidoc

- [Why do we need AsciiDoc](https://asciidoctor.org/docs/what-is-asciidoc/) ?
- [Asciidoctor reveal.js - Asciidoctor reveal.js Documentation](https://docs.asciidoctor.org/reveal.js-converter/latest/)
